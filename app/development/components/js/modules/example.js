export default class $example {
	constructor(init) {
		this.selector = document.querySelector(init.burger);
		if (!this.selector) return;
		
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('MODULE:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.main();
		}
	}
	
	main() {
		this.lorem = `
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		`;

		console.log(this.lorem);
	}
}
