/* ==== IMPORT PARAMS ==== */


module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
	() => combiner(
		src(`${__init.inPub}/css/global.css`, { allowEmpty: true }),
		_run.if(__init.isPublic, _run.csso()),
		_run.rename({ suffix: '.min' }),
		dest(__init.inPubCss)
	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
