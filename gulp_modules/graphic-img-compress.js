/* ==== IMPORT PARAMS ==== */
'use strict';
import {optipng, gifsicle, jpegtran} from 'gulp-imagemin';
 
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inDev = 'development',
inDevApps = `${inDev}/components`,
inPub = 'public';
/* ==== ----- ==== */


/* ==== Replace URL or Links ==== */
const __cfg = {
	imagemin: [
		gifsicle({interlaced: true}),
		jpegtran({progressive: true}),
		optipng({optimizationLevel: 2}),
	]
};
/* ==== ----- ==== */


module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
	() => combiner(
	
			src(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`),
			// _run.newer(`${inPub}/media/img/`),
			// _run.cached('imgcache'),
			_run.if(isDevelopment, _run.imagemin(__cfg.imagemin)),
			_run.if(isDevelopment, _run.filesize()),
			// _run.remember('imgcache'),
			dest(`${inPub}/media/img`)
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));

