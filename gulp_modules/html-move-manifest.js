/* ==== IMPORT PARAMS ==== */
import { lastRun } from 'gulp';
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const inDev = 'development';
const inDevApps = `${inDev}/components`;
const inPub = 'public';
const folder = 'manifest';
/* ==== ----- ==== */

/* ==== Replace URL or Links ==== */

const __cfg = {
	manifest: {
		arr: [
			`${inDevApps}/${folder}/manifest192.png`,
			`${inDevApps}/${folder}/manifest512.png`,
			`${inDevApps}/${folder}/manifest.json`
		]
	}
};

/* ==== ----- ==== */

const sinceReplace = x => (`${x}`.replace(/-/gi, ':'));

module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
	() => combiner(
		src(__cfg.manifest.arr, { allowEmpty: true, since: lastRun(sinceReplace(nameTask)) }),
		_run.newer(`${inPub}/`),
		dest(`${inPub}/${folder}/`)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
