/* ==== IMPORT PARAMS ==== */

import fs from 'fs';
import path from 'path';

import someHTTP2 from 'http2';
// import http2module from 'spdy-or-http2';

// const requireResolve = require('resolve');

// // get molnarg's http2 module
// const http2Sync = require(requireResolve.sync('http2', { basedir: process.cwd() }));
const resolve = require('resolve');

resolve.sync('http2'); // will resolve the molnarg http2 module if you have it installed
require.resolve('http2'); // will always resolve the native http2 module

// var transport = require('spdy-transport');

// NOTE: socket is some stream or net.Socket instance, may be an argument
// of `net.createServer`'s connection handler.

// var server = transport.connection.create(socket, {
// 	protocol: 'http2',
// 	isServer: true
// });

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */ 
const inPub = 'public';
/* ==== ----- ==== */

const browserSync = require('browser-sync').create();

const PATHS = {
	sync: path.join(`${__dirname}\\..\\${inPub}\\404.html`)
};
/* ==== Replace URL or Links ==== */
const __cfg = {
	browserSync: {
		server: {
			baseDir: inPub,
			middleware: (req, res, next) => {
				res.setHeader('Access-Control-Allow-Origin', '*');
				next();
			}
		},
		http2: true,
		httpModule: someHTTP2,
		// httpModule: 'http2',
		https: {
			cert: `${process.env.LOCALAPPDATA}\\mkcert\\localhost.pem`,
			key: `${process.env.LOCALAPPDATA}\\mkcert\\localhost-key.pem`
		},
		
		port: 3000,
		injectChanges: true,
		ghostMode: false,
		notify: false,
		open: false,
		browser: 'chrome.exe',
		online: true
	}
};
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => (
		browserSync.init(__cfg.browserSync, (err, bs) => {
			bs.addMiddleware('*', (req, res) => {
				res.write(fs.readFileSync(PATHS.sync));
				res.end();
			});
		}),
		browserSync.watch(`${inPub}/**/*.*`).on('change', browserSync.reload)
	).on('error',
		_run.notify.onError(err => (errorConfig(`task: ${nameTask} `, 'ошибка!', err))));
